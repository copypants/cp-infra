#!/bin/bash
docker build -t copypants/cp-infra:base ../base
docker build -t copypants/cp-infra:onbuild ../onbuild
docker build -t copypants/cp-infra:devbuild ../devbuild
docker build -t copypants/cp-infra:binbuild ../binbuild