set -e

chmod -R 777 /root

export USER=root

if [ -d /bundle ]; then
  cd /bundle
  tar xzf *.tar.gz
  cd /bundle/bundle/
  npm i
elif [[ $BUNDLE_URL ]]; then
  cd /tmp
  curl -L -o bundle.tar.gz $BUNDLE_URL
  tar xzf bundle.tar.gz
  cd /tmp/bundle/
  npm i
elif [ -d /built_app ]; then
  cd /built_app
else
  echo "=> You don't have an app to run in this image."
  exit 1
fi

if [ -f ./install-extras.sh ]; then
  echo "Installing extras..."
  bash ./install-extras.sh
fi

if [[ $REBUILD_NPM_MODULES ]]; then
  if [ -f /opt/cp-infra/rebuild_npm_modules.sh ]; then
    bash /opt/cp-infra/rebuild_npm_modules.sh
  else
    echo "=> Use copypants/cp-infra:bin-build for binary bulding."
    exit 1
  fi
fi

# Set a delay to wait to start container
if [[ $DELAY ]]; then
  echo "Delaying startup for $DELAY seconds"
  sleep $DELAY
fi

# Honour already existing PORT setup
export PORT=${PORT:-80}

echo "=> Starting app on port:$PORT"
xvfb-run -a node bootstrap.js
