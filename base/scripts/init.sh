set -e

bash $CPINFRA_DIR/lib/install_base.sh
bash $CPINFRA_DIR/lib/install_node.sh
bash $CPINFRA_DIR/lib/install_xvfb.sh
bash $CPINFRA_DIR/lib/cleanup.sh
