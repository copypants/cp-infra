set -e

BUNDLE_DIR=/tmp/bundle-dir

cp -R /app $BUNDLE_DIR
cd $BUNDLE_DIR

npm i

mv $BUNDLE_DIR /built_app

# cleanup
rm -rf $BUNDLE_DIR
