#!/bin/bash
set -e
apt-get update -y
apt-get upgrade -y
apt-get install -y curl bzip2 build-essential python git apt-utils

chmod 777 /root
